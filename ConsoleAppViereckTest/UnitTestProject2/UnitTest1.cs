﻿using System;
using ConsoleAppViereckTest;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {

            Viereck testee1 = new Viereck(24, 42);

            Viereck testee2 = new Viereck(42, 24.0015);

            Assert.AreEqual(testee1, testee2);
        }
    }
}
