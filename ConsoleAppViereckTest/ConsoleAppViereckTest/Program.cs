﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppViereckTest
{
    public class Viereck
    {
        private double a;
        private double b;
        private double tolerance = 0.5;

        static void Main(string[] args)
        {
        }
        
        public Viereck(double a, double b)
        {
            this.a = a;
            this.b = b;
        }
        
        public override bool Equals(object obj)
        {

            Viereck tmp = (Viereck) obj;
 
            if ((this.a == tmp.a) && (this.b == tmp.b) || (this.a == tmp.b) && (this.b == tmp.a))
            {
                return true;
            }

            if ((Math.Abs(this.a -tmp.a ) < tolerance) && (Math.Abs(this.b - tmp.b) < tolerance) || (Math.Abs(this.a - tmp.b) < tolerance) && (Math.Abs(this.b - tmp.a) < tolerance))
            {
                return true;
            }

            return false;
        }
    }
}
