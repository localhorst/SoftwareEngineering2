﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleAppLock
{
    class Program
    {
        private static List<int> intlist;
        private static object lockobject;
        private static ReaderWriterLockSlim rwls;

        static void Main(string[] args)
        {

         Task t = Task.Run(() =>
                {
                    int i = 5;
                    int b = 2 * i;
                    Console.WriteLine("Result: " + b);
                }
            );

       //  t.Start(); //geht net nochmals

            foreach (var item in getList())
            {
                Console.WriteLine(item);
            }


            /*

            intlist = new List<int>();
            lockobject = new object();

            rwls = new ReaderWriterLockSlim();

            while (true)
            {
                int intTemp;

                try
                {
                     intTemp = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception e)
                {
                    continue;
                }
               

                if (containsNumber(intTemp))
                {
                    Console.WriteLine("Add");
                    addNumber(intTemp);
                }
                else
                {
                    Console.WriteLine("Not Added");
                }

                printNumbers();
            }
            */

            Console.Read();
        }

        static bool containsNumber(int i)
        {
            bool result = true;
            //lock (lockobject)
            rwls.EnterReadLock();
            {
                try
                {
                    if (intlist.Contains(i))
                    {
                        result = false;
                    }
                }
                finally
                {
                    rwls.ExitReadLock();
                }
            }
            // rwls.ExitReadLock();
            return result;
        }

        static void addNumber(int i)
        {
            //lock (lockobject)
            rwls.EnterWriteLock();
            {
                intlist.Add(i);
            }
            rwls.ExitWriteLock();
        }


        static void printNumbers()
        {
            Console.Write("Numbers: ");

            //lock (lockobject)
            rwls.EnterReadLock();
            {
                foreach (var VARIABLE in intlist)
                {
                    Console.Write(VARIABLE + " ");
                }
            }
            rwls.ExitReadLock();
            Console.WriteLine("");
        }


        static IEnumerable<string> getList()
        {

            Console.WriteLine("started");
            // List<int> intlist = new List<int>();
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("generate List");
                //intlist.Add(i);
                yield return i.ToString();
            }

            Console.WriteLine("finished");

            //return intlist;
        }
    }
}