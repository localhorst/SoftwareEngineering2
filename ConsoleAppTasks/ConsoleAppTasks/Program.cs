﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleAppTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            DoIt3();

            
            List<Task> list = new List<Task>();

            for (int i = 0; i < 10; i++)
            {
                /*
                Task t = new Task(() =>
                {
                    Thread.Sleep(100); 
                    Console.WriteLine(".");
                });
                */
                Task t = new Task(simple);


                list.Add(t);

            }


            foreach (var VARIABLE in list)
            {
              //  VARIABLE.Start();
            }

           // Task.WaitAll(list.ToArray());

            Console.WriteLine("finished");

            //var t = new Task(DoIt);

            //DoIt2();
            
            for (int i = 0; i < int.MaxValue; i++)
            {
                Console.Write(".");
                Thread.Sleep(250);
            }

            /*
            int i = 4;
            int y = 0;

            //anonyme methode
            var t = new Task((number) =>
            {
                Thread.Sleep(2000);
                Console.WriteLine("2");
                y = 2 * (int) number;
            }, i);

            t.Start();

            i = 99;

            t.Wait();

            /*
           for (int i = 0; i < 999; i++)
           {
               Console.WriteLine("-");
               Thread.Sleep(200);
            }

    */

            //Console.WriteLine(y); //198 and with number = 8

            Console.ReadLine(); //wait for close
        }


        private static void DoIt()
        {
            Console.WriteLine(".");

            for (int i = 0; i < 999; i++)
            {
                Console.Write("+");
                Thread.Sleep(250);
            }
        }

        private static async void DoIt2()
        {
            int i = 4;
            int y = 0;
            /*
            //anonyme methode
            var t = new Task((number) =>
            {
                Thread.Sleep(2000);
                Console.WriteLine("2");
                y = 2 * (int) number;
                Console.WriteLine(y); //198 and with number = 8

                Console.WriteLine("task finished");

            }, i);

    */
            Task<int> t = new Task<int>(dosomething);
        


            t.Start();
            i = 99;
            //t.Wait();

            await t;

            Console.WriteLine(t.Result); //result wartet auf den task

            Console.WriteLine("result: " + y);

        }

      private  static  int dosomething()
        {
            int res = 0;
            for (int i = 0; i < 321; i++)
            {
                res++;
            }
            Thread.Sleep(200);
            return res;
        }

      public static void simple()
      {


          int ii = 0;

          Console.WriteLine("vor");

          int i = 5 / ii;

          Thread.Sleep(200);
          Console.WriteLine("finished /0");
        }

      private static void DoIt3()
      {
          var t = new Task(simple);
          t.Start();
          //t.Exception gab es eine expetion??
          t.Wait(); //is now main thread expetion

          Console.WriteLine("Hello");
      }
      

    }



}