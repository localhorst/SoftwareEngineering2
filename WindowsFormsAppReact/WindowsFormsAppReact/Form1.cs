﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAppReact
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Subject.Sample(TimeSpan.FromMilliseconds(20)).ObserveOn(SynchronizationContext.Current).Subscribe((i) =>
            {
                label1.Text = i.ToString();
            });

            spam();
        }

        public Subject<int> Subject = new Subject<int>();

        public void spam()
        {
            Task.Run(() =>
                {
                    for (int i = 0; i < 10000000000; i++)
                    {
                        Subject.OnNext(i);
                    }
                }
            );
        }
    }
}