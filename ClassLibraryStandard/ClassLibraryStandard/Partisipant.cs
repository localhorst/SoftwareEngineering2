﻿using System;

namespace ClassLibraryStandard
{
    public class Participant
    {


        public string name { get; set; }
        public string lastname { get; set; }
        public string geburtstag { get; set; }

        public Participant()
        {
            geburtstag = DateTime.Today.ToString();
        }

        public override string ToString()
        {
            return name.ToString() + " " + lastname.ToString();
        }
    }
}
