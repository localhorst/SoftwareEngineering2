﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using ClassLibraryStandard;

namespace EntitiesStandard
{
    public static class XMLhelper
    {


        public static void serializeToXML<T>(T list )
        {
          

            XmlSerializer ser = new XmlSerializer(typeof(List<Participant>));

            FileStream file =  File.OpenWrite(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\data.xml");

            ser.Serialize(file, list);

            file.Close();
        }

        public static T xmlToDeserialize<T>()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            FileStream file =  File.OpenRead(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\data.xml");

            T blogList = (T) serializer.Deserialize(file);

            //T value = (T) Convert.ChangeType(serializer.Deserialize(file));

            file.Close();
            return blogList;
        }

    }
}
