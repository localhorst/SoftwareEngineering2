﻿using System;
using System.Collections.Generic;
using System.Threading;
using ClassLibraryStandard;
using EntitiesStandard;

namespace Service
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World");

            List<Participant> parlis = new List<Participant>();

            Participant pr1 = new Participant();
            pr1.name = "Hans1";
            pr1.lastname = "Wurst1";
            parlis.Add(pr1);

            Participant pr2 = new Participant();
            pr2.name = "Hans2";
            pr2.lastname = "Wurst2";
            parlis.Add(pr2);

            Participant pr3 = new Participant();
            pr3.name = "Hans3";
            pr3.lastname = "Wurst3";
            parlis.Add(pr3);

            Participant pr4 = new Participant();
            pr4.name = "Hans4";
            pr4.lastname = "Wurst4";
            parlis.Add(pr4);

            while (true)
            {
                Thread.Sleep(500);


                Random ran = new Random();
                int tmp = ran.Next(0, 3);

                Console.WriteLine(parlis[tmp]);

                XMLhelper.serializeToXML<List<Participant>>(parlis);
                
            }
        }
    }
}