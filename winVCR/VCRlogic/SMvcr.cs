﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VCRlogic;


public static class SMvcr
{
    private static StateVCR _concreteState;

    public delegate void VCREventHandler();

    public static event VCREventHandler evStopbutton;

    public static event VCREventHandler evEndtape;

    public static event VCREventHandler evRecordbutton;

    public static event VCREventHandler evPlaybutton;

    public static event VCREventHandler evBeginningtape;

    public static event VCREventHandler evRewindbutton;

    public static event VCREventHandler evFastforwardbutton;


    public delegate void StatusLabelHandler(StatusEventArgs e);

    public static event StatusLabelHandler evUpdateStatus;


    public static void initSMvcr()
    {
        setState(new SvcrStandBy());
    }

    public static void triggerUpdateLabel(string txt)
    {
        var e = new StatusEventArgs(txt);
        //trigger handler

        if (evUpdateStatus != null)
        {
            evUpdateStatus(e);
        }
    }

    public static void setState(StateVCR e)
    {
        _concreteState = null;
        _concreteState = e;
    }

    public static void stop()
    {
        if (evStopbutton != null)
        {
            evStopbutton();
        }
    }

    public static void record()
    {
        if (SMtape.getState() is StapeTapeInNotEnd)
        {
            if (evRecordbutton != null)
            {
                evRecordbutton();
            }
        }
    }

    public static void play()
    {
        if (SMtape.getState() is StapeTapeInNotEnd)
        {
            if (evPlaybutton != null)
            {
                evPlaybutton();
            }
        }
    }

    public static void rewind()
    {
        if (SMtape.getState() is StapeTapeInEnd)
        {
            if (evRewindbutton != null)
            {
                evRewindbutton();
            }
        }
    }

    public static void fastforward()
    {
        if (SMtape.getState() is StapeTapeInNotEnd)
        {
            if (evFastforwardbutton != null)
            {
                evFastforwardbutton();
            }
        }
    }


    public static void tapeEnd()
    {
        if (evEndtape != null)
        {
            evEndtape();
        }
    }

    public static void tapeBeginn()
    {
        if (evBeginningtape != null)
        {
            evBeginningtape();
        }
    }
}


public abstract class StateVCR
{
    public abstract void evStopbutton();
    public abstract void evEndtape();
    public abstract void evRecordbutton();
    public abstract void evPlaybutton();
    public abstract void evBeginningtape();
    public abstract void evRewindbutton();
    public abstract void evFastforwardbutton();
}

public class SvcrStandBy : StateVCR
{
    public SvcrStandBy()
    {
        Debug.Print("Standby\n");

        SMvcr.triggerUpdateLabel("Standby");

        SMvcr.evRecordbutton += evRecordbutton;
        SMvcr.evPlaybutton += evPlaybutton;
        SMvcr.evRewindbutton += evRewindbutton;
    }

    public override void evStopbutton()
    {
    }

    public override void evEndtape()
    {
    }

    public override void evRecordbutton()
    {
        SMvcr.evRecordbutton -= evRecordbutton;
        SMvcr.evPlaybutton -= evPlaybutton;
        SMvcr.evRewindbutton -= evRewindbutton;

        SMvcr.setState(new SvcrRecord());
    }

    public override void evPlaybutton()
    {
        SMvcr.evRecordbutton -= evRecordbutton;
        SMvcr.evPlaybutton -= evPlaybutton;
        SMvcr.evRewindbutton -= evRewindbutton;

        SMvcr.setState(new SvcrPlay());
    }

    public override void evBeginningtape()
    {
    }

    public override void evRewindbutton()
    {
        SMvcr.evRecordbutton -= evRecordbutton;
        SMvcr.evPlaybutton -= evPlaybutton;
        SMvcr.evRewindbutton -= evRewindbutton;

        SMvcr.setState(new SvcrRewind());
    }

    public override void evFastforwardbutton()
    {
    }
}

public class SvcrPlay : StateVCR
{
    public SvcrPlay()
    {
        Debug.Print("Play\n");



        SMvcr.triggerUpdateLabel("Play");

        SMvcr.evStopbutton += evStopbutton;
        SMvcr.evEndtape += evEndtape;
        SMvcr.evRewindbutton += evRewindbutton;
        SMvcr.evFastforwardbutton += evFastforwardbutton;
    }

    public override void evStopbutton()
    {
        SMvcr.evStopbutton -= evStopbutton;
        SMvcr.evEndtape -= evEndtape;
        SMvcr.evRewindbutton -= evRewindbutton;
        SMvcr.evFastforwardbutton -= evFastforwardbutton;

        SMvcr.setState(new SvcrStandBy());
    }

    public override void evEndtape()
    {
        SMvcr.evStopbutton -= evStopbutton;
        SMvcr.evEndtape -= evEndtape;
        SMvcr.evRewindbutton -= evRewindbutton;
        SMvcr.evFastforwardbutton -= evFastforwardbutton;

        SMvcr.setState(new SvcrStandBy());
    }

    public override void evRecordbutton()
    {
    }

    public override void evPlaybutton()
    {
    }

    public override void evBeginningtape()
    {
    }

    public override void evRewindbutton()
    {
        SMvcr.evStopbutton -= evStopbutton;
        SMvcr.evEndtape -= evEndtape;
        SMvcr.evRewindbutton -= evRewindbutton;
        SMvcr.evFastforwardbutton -= evFastforwardbutton;

        SMvcr.setState(new SvcrRewind());
    }

    public override void evFastforwardbutton()
    {
        SMvcr.evStopbutton -= evStopbutton;
        SMvcr.evEndtape -= evEndtape;
        SMvcr.evRewindbutton -= evRewindbutton;
        SMvcr.evFastforwardbutton -= evFastforwardbutton;

        SMvcr.setState(new SvcrFastForward());
    }
}

public class SvcrRewind : StateVCR
{
    public SvcrRewind()
    {
        Debug.Print("Rewind\n");

        SMvcr.triggerUpdateLabel("Rewind");

        SMvcr.evStopbutton += evStopbutton;
        SMvcr.evBeginningtape += evBeginningtape;
        SMvcr.evPlaybutton += evPlaybutton;
        SMvcr.evFastforwardbutton += evFastforwardbutton;
    }

    public override void evStopbutton()
    {
        SMvcr.evStopbutton -= evStopbutton;
        SMvcr.evBeginningtape -= evBeginningtape;
        SMvcr.evPlaybutton -= evPlaybutton;
        SMvcr.evFastforwardbutton -= evFastforwardbutton;

        SMvcr.setState(new SvcrStandBy());
    }

    public override void evEndtape()
    {
    }

    public override void evRecordbutton()
    {
    }

    public override void evPlaybutton()
    {
        SMvcr.evStopbutton -= evStopbutton;
        SMvcr.evBeginningtape -= evBeginningtape;
        SMvcr.evPlaybutton -= evPlaybutton;
        SMvcr.evFastforwardbutton -= evFastforwardbutton;

        SMvcr.setState(new SvcrPlay());
    }

    public override void evBeginningtape()
    {
        SMvcr.evStopbutton -= evStopbutton;
        SMvcr.evBeginningtape -= evBeginningtape;
        SMvcr.evPlaybutton -= evPlaybutton;
        SMvcr.evFastforwardbutton -= evFastforwardbutton;

        SMvcr.setState(new SvcrStandBy());
    }

    public override void evRewindbutton()
    {
    }

    public override void evFastforwardbutton()
    {
        SMvcr.evStopbutton -= evStopbutton;
        SMvcr.evBeginningtape -= evBeginningtape;
        SMvcr.evPlaybutton -= evPlaybutton;
        SMvcr.evFastforwardbutton -= evFastforwardbutton;

        SMvcr.setState(new SvcrFastForward());
    }
}

public class SvcrFastForward : StateVCR
{
    public SvcrFastForward()
    {
        Debug.Print("FastForward\n");

        SMvcr.triggerUpdateLabel("FastForward");

        SMvcr.evStopbutton += evStopbutton;
        SMvcr.evEndtape += evEndtape;
        SMvcr.evRewindbutton += evRewindbutton;
    }

    public override void evStopbutton()
    {
        SMvcr.evStopbutton -= evStopbutton;
        SMvcr.evEndtape -= evEndtape;
        SMvcr.evRewindbutton -= evRewindbutton;

        SMvcr.setState(new SvcrStandBy());
    }

    public override void evEndtape()
    {
        SMvcr.evStopbutton -= evStopbutton;
        SMvcr.evEndtape -= evEndtape;
        SMvcr.evRewindbutton -= evRewindbutton;

        SMvcr.setState(new SvcrStandBy());
    }

    public override void evRecordbutton()
    {
    }

    public override void evPlaybutton()
    {
    }

    public override void evBeginningtape()
    {
    }

    public override void evRewindbutton()
    {
        SMvcr.evStopbutton -= evStopbutton;
        SMvcr.evEndtape -= evEndtape;
        SMvcr.evRewindbutton -= evRewindbutton;

        SMvcr.setState(new SvcrRewind());
    }

    public override void evFastforwardbutton()
    {
    }
}

public class SvcrRecord : StateVCR
{
    public SvcrRecord()
    {
        Debug.Print("Record\n");

        SMvcr.triggerUpdateLabel("Record");

        SMvcr.evStopbutton += evStopbutton;
        SMvcr.evEndtape += evEndtape;
    }

    public override void evStopbutton()
    {
        SMvcr.evStopbutton -= evStopbutton;
        SMvcr.evEndtape -= evEndtape;

        SMvcr.setState(new SvcrStandBy());
    }

    public override void evEndtape()
    {
        SMvcr.evStopbutton -= evStopbutton;
        SMvcr.evEndtape -= evEndtape;

        SMvcr.setState(new SvcrStandBy());
    }

    public override void evRecordbutton()
    {
    }

    public override void evPlaybutton()
    {
    }

    public override void evBeginningtape()
    {
    }

    public override void evRewindbutton()
    {
    }

    public override void evFastforwardbutton()
    {
    }
}

public class StatusEventArgs
{
    public StatusEventArgs(string txt)
    {
        Text = txt;
    }

    public string Text { get; private set; }
}