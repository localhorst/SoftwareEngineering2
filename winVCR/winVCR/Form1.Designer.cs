﻿namespace winVCR
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnPlay = new System.Windows.Forms.Button();
            this.Rewind = new System.Windows.Forms.Button();
            this.btnRecord = new System.Windows.Forms.Button();
            this.statusLabel = new System.Windows.Forms.Label();
            this.btnInsertTape = new System.Windows.Forms.Button();
            this.btnEjectTape = new System.Windows.Forms.Button();
            this.btnEndTape = new System.Windows.Forms.Button();
            this.btnNotEndTape = new System.Windows.Forms.Button();
            this.btnFastForward = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(38, 89);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(720, 239);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnPlay
            // 
            this.btnPlay.BackColor = System.Drawing.Color.Transparent;
            this.btnPlay.Location = new System.Drawing.Point(625, 102);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(75, 23);
            this.btnPlay.TabIndex = 1;
            this.btnPlay.Text = "Play";
            this.btnPlay.UseVisualStyleBackColor = false;
            this.btnPlay.Click += new System.EventHandler(this.BtnPlay_Click);
            // 
            // Rewind
            // 
            this.Rewind.Location = new System.Drawing.Point(586, 159);
            this.Rewind.Name = "Rewind";
            this.Rewind.Size = new System.Drawing.Size(75, 23);
            this.Rewind.TabIndex = 3;
            this.Rewind.Text = "Rewind";
            this.Rewind.UseVisualStyleBackColor = true;
            this.Rewind.Click += new System.EventHandler(this.Rewind_Click);
            // 
            // btnRecord
            // 
            this.btnRecord.BackColor = System.Drawing.Color.DarkRed;
            this.btnRecord.Location = new System.Drawing.Point(706, 89);
            this.btnRecord.Name = "btnRecord";
            this.btnRecord.Size = new System.Drawing.Size(52, 25);
            this.btnRecord.TabIndex = 4;
            this.btnRecord.Text = "Record";
            this.btnRecord.UseVisualStyleBackColor = false;
            this.btnRecord.Click += new System.EventHandler(this.BtnRecord_Click);
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Font = new System.Drawing.Font("Arial", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusLabel.Location = new System.Drawing.Point(267, 223);
            this.statusLabel.MinimumSize = new System.Drawing.Size(200, 50);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(200, 55);
            this.statusLabel.TabIndex = 5;
            this.statusLabel.Text = "Standby";
            this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnInsertTape
            // 
            this.btnInsertTape.Location = new System.Drawing.Point(197, 117);
            this.btnInsertTape.Name = "btnInsertTape";
            this.btnInsertTape.Size = new System.Drawing.Size(79, 44);
            this.btnInsertTape.TabIndex = 6;
            this.btnInsertTape.Text = "Insert Tape";
            this.btnInsertTape.UseVisualStyleBackColor = true;
            this.btnInsertTape.Click += new System.EventHandler(this.BtnInsertTape_Click);
            // 
            // btnEjectTape
            // 
            this.btnEjectTape.Location = new System.Drawing.Point(483, 117);
            this.btnEjectTape.Name = "btnEjectTape";
            this.btnEjectTape.Size = new System.Drawing.Size(79, 44);
            this.btnEjectTape.TabIndex = 7;
            this.btnEjectTape.Text = "Eject Tape";
            this.btnEjectTape.UseVisualStyleBackColor = true;
            this.btnEjectTape.Click += new System.EventHandler(this.BtnEjectTape_Click);
            // 
            // btnEndTape
            // 
            this.btnEndTape.Location = new System.Drawing.Point(293, 117);
            this.btnEndTape.Name = "btnEndTape";
            this.btnEndTape.Size = new System.Drawing.Size(79, 44);
            this.btnEndTape.TabIndex = 8;
            this.btnEndTape.Text = "End Tape";
            this.btnEndTape.UseVisualStyleBackColor = true;
            this.btnEndTape.Click += new System.EventHandler(this.BtnEndTape_Click);
            // 
            // btnNotEndTape
            // 
            this.btnNotEndTape.Location = new System.Drawing.Point(388, 117);
            this.btnNotEndTape.Name = "btnNotEndTape";
            this.btnNotEndTape.Size = new System.Drawing.Size(79, 44);
            this.btnNotEndTape.TabIndex = 9;
            this.btnNotEndTape.Text = "Not End Tape";
            this.btnNotEndTape.UseVisualStyleBackColor = true;
            this.btnNotEndTape.Click += new System.EventHandler(this.BtnNotEndTape_Click);
            // 
            // btnFastForward
            // 
            this.btnFastForward.Location = new System.Drawing.Point(667, 159);
            this.btnFastForward.Name = "btnFastForward";
            this.btnFastForward.Size = new System.Drawing.Size(75, 23);
            this.btnFastForward.TabIndex = 10;
            this.btnFastForward.Text = "FastForward";
            this.btnFastForward.UseVisualStyleBackColor = true;
            this.btnFastForward.Click += new System.EventHandler(this.BtnFastForward_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.Color.Transparent;
            this.btnStop.Location = new System.Drawing.Point(625, 130);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 11;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.BtnStop_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnFastForward);
            this.Controls.Add(this.btnNotEndTape);
            this.Controls.Add(this.btnEndTape);
            this.Controls.Add(this.btnEjectTape);
            this.Controls.Add(this.btnInsertTape);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.btnRecord);
            this.Controls.Add(this.Rewind);
            this.Controls.Add(this.btnPlay);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Button Rewind;
        private System.Windows.Forms.Button btnRecord;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.Button btnInsertTape;
        private System.Windows.Forms.Button btnEjectTape;
        private System.Windows.Forms.Button btnEndTape;
        private System.Windows.Forms.Button btnNotEndTape;
        private System.Windows.Forms.Button btnFastForward;
        private System.Windows.Forms.Button btnStop;
    }
}

