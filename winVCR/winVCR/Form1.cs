﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using VCRlogic;



namespace winVCR
{
    public partial class Form1 : Form
    {
       
        public Form1()
        {
            InitializeComponent();

            SMtape.initSMtape();
            SMvcr.initSMvcr();

            SMvcr.evUpdateStatus += setLabelText;

        }




        public void setLabelText(StatusEventArgs e)
        {
            statusLabel.Text = e.Text;
        }


        private void BtnPlay_Click(object sender, EventArgs e)
        {

                SMvcr.play();
        }

        private void Rewind_Click(object sender, EventArgs e)
        {
            SMvcr.rewind();
        }

        private void BtnRecord_Click(object sender, EventArgs e)
        {
            SMvcr.record();
        }

        private void Power_Click(object sender, EventArgs e)
        {
        }

        private void BtnInsertTape_Click(object sender, EventArgs e)
        {
            SMtape.insertTape();
        }

        private void BtnEjectTape_Click(object sender, EventArgs e)
        {
            SMtape.ejectTape();
        }

        private void BtnNotEndTape_Click(object sender, EventArgs e)
        {
            SMtape.notEndTape();
        }

        private void BtnEndTape_Click(object sender, EventArgs e)
        {
SMtape.endTape();
        }

        private void BtnFastForward_Click(object sender, EventArgs e)
        {
            SMvcr.fastforward();
        }

        private void BtnStop_Click(object sender, EventArgs e)
        {
            SMvcr.stop();
        }
    }
}