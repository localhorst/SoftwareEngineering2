﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleAppReact
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            var calc = new Calc();
            calc.Subject.Subscribe((i) => { Console.WriteLine("RX: " + i); });
            calc.Subject.Throttle(TimeSpan.FromSeconds(10)).Subscribe((i) => { Console.WriteLine("RX (Throttle): " + i); });
            calc.add(1, 5);
            */

            var d = new DoIt();

            /*
            d.DoIt2(1,2).Subscribe((i) =>
                { Console.WriteLine("RX (doit2): " + i); });


            d.DoIt2(1, 2).Sample(TimeSpan.Zero).Subscribe((i) => { Console.WriteLine("RX (doit2, Sample): " + i); });

    */

            d.DoIt2(1, 2).Delay(TimeSpan.FromSeconds(2)).Subscribe((i) => { Console.WriteLine("RX (doit2, Delay(2)): " + i); });

            Console.Read();
        }
    }

    public class DoIt
    {
        public IObservable<int> DoIt2(int i1, int i2)
        {
            var calc = new Calc();
            calc.add(1, 5);

            return calc.Subject;
        }
    }

    public class Calc
    {
        //public Subject<int> Subject = new Subject<int>();

        public ReplaySubject<int> Subject = new ReplaySubject<int>();

        public void add(int i1, int i2)
        {
            int res = 0;

            for (int i = 0; i < 1000; i++)
            {
                res += i1 + i2;
                Subject.OnNext(res);
            }
        }
    }
}