﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsAppTask
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private Task t;
        private CancellationTokenSource cts;

        private void Button1_Click(object sender, EventArgs e)
        {
            cts = new CancellationTokenSource();
            t = new Task(DoIt, cts.Token);
            t.Start();

            for (var i = 0; i < 999; i++)
            {
                textBox1.Text += "-";
                Application.DoEvents(); //update ui elements
                Thread.Sleep(100);
            }
        }


        private void DoIt()
        {
            //Console.WriteLine(".");

            for (var i = 0; i < 999; i++)
            {
                if (cts.IsCancellationRequested)
                {
                    return;
                }
                try
                {
                    if (textBox1.InvokeRequired)
                        textBox1.Invoke(new MethodInvoker(() => { textBox1.Text += "x"; }));
                    else
                        textBox1.Text += "x";
                }
                catch (ObjectDisposedException)
                {
                    return;
                }

                Thread.Sleep(200);
            }
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            cts.Cancel();

            for (var i = 0; i < 999; i++)
            {
                var ii = 0;
            }
        }
    }
}