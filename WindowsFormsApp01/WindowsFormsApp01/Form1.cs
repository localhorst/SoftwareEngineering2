﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary01;

namespace WindowsFormsApp01
{
    public partial class Form1 : Form
    {
        private int _number;

        private int _scale = 1;
        private int _x;
        private int _y;

        public int Number
        {
            get { return _number; }
            private set
            {
                if (value > 10)
                {
                    _number = 10;
                }
                else
                {
                    _number = value;
                }
            }
        }

        public int GetNumber()
        {
            //  int number2 = 10;

            var foo = new Form1();
            foo.Name = "hallo";
            foo.Number = 5;


            return Number;
        }

        public Form1()
        {
            InitializeComponent();
            label1.Text = " ";

            calc.CalcFinished += CalcFinished;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            var num1 = numericUpDown1.Value;
            var num2 = numericUpDown2.Value;


            //var res = num2 + num1;

            var foo = new ClassLibrary01.Class1();

            var res = foo.Add((int) num1, (int) num2);

            _number = (int) res;

            label1.Text = "Ergebnis: " + res.ToString();


            var f = new CalcEventArgs(res);
            //trigger handler

            // if (CalcFinished != null)
            //  {
            CalcFinished(f);
            // }
        }


        private void Panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;

            Pen _pen = new ClassLibrary01.Class1().getPen();

            using (var p = _pen)
            {
                draw(graphics);
            }

            Brush aBrush = (Brush) Brushes.Red;


            for (int i = 10; i < (502); i++)
            {
                int y = (int) (246 - (((Math.Log(i) * 10) - 23) * _scale));

                graphics.FillRectangle(aBrush, i, y, 2, 2);
            }
        }

        private void storeToFile()
        {
            Bitmap bmp = new Bitmap(100, 200);

            var graphics = Graphics.FromImage(bmp);

            draw(graphics);

            bmp.Save(@"C:\Temp\AI.png");
        }

        private void draw(Graphics graphics)
        {
            Pen _pen = new ClassLibrary01.Class1().getPen();

            graphics.Clear(Color.Aqua);
            

            using (var p = _pen)
            {
                //panel1.Width

                //y axis
                graphics.DrawLine(p, 10, 10, 10, 246);


                //x axis
                graphics.DrawLine(p, 10, 246, 502, 246);

                //arrow y axis
                graphics.DrawLine(p, 5, 15, 10, 10);
                graphics.DrawLine(p, 15, 15, 10, 10);

                //arrow x axis
                graphics.DrawLine(p, 498, 241, 502, 246);
                graphics.DrawLine(p, 498, 251, 502, 246);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void TrackBar1_Scroll(object sender, EventArgs e)
        {
            _scale = (int) trackBar1.Value;

            panel1.Refresh();
        }

        private void Panel1_AutoSizeChanged(object sender, EventArgs e)
        {
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            _x = panel1.Width;
            _y = panel1.Height;
            panel1.Invalidate();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            string s1 = "hello";

            string s2 = "Offenburg";

            string s3 = String.Empty;

            Stopwatch sw = new Stopwatch();

            sw.Start();

            for (int i = 0; i < 50000; i++)
            {
                s3 = s3 + s1 + s2;
            }

            sw.Stop();

            label3.Text = sw.ElapsedMilliseconds.ToString();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            string s1 = "hello";

            string s2 = "Offenburg";

            string s3 = String.Empty;

            Stopwatch sw = new Stopwatch();
            var sb = new StringBuilder();

            sw.Start();

            for (int i = 0; i < 50000; i++)
            {
                //s3 = s3 + s1 + s2;

                sb.Append(s1);
                sb.Append(s2);
            }

            sw.Stop();

            label2.Text = sw.ElapsedMilliseconds.ToString();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            try
            {
                calc.CalcIt();
            }
            catch (DivideByZeroException)
            {
                label4.Text = "Fehler: div by zero";
            }
            catch (Exception)
            {
                label4.Text = "Fehler";
            }
        }

        public void CalcFinished(CalcEventArgs e)
        {
            label4.Text = e.Result.ToString();
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            storeToFile();
        }
    }

    public class CalcEventArgs
    {
        public CalcEventArgs(int result)
        {
            Result = result;
        }

        public int Result { get; private set; }
    }


    public static class calc
    {
        public delegate void CalcEventHandler(CalcEventArgs e);

        public static event CalcEventHandler CalcFinished;


        public static void CalcIt()
        {
            int res = DateTime.Now.Second;

            res = res / 0;

            var e = new CalcEventArgs(res);
            //trigger handler

            if (CalcFinished != null)
            {
                CalcFinished(e);
            }
        }


        public static int ADD(int i1, int i2)
        {
            int res = i1 + i2;

            return res;


        }
    }
}