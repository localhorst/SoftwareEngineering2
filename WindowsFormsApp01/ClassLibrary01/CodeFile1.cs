﻿
using System.Runtime.CompilerServices;

public class SingeltonPattern
{



    private static SingeltonPattern _singelton;


    private SingeltonPattern()
    {

    }


    public static SingeltonPattern GetInstance()
    {
        if (_singelton == null)
        {
            _singelton = new SingeltonPattern();
        }

        return _singelton;

    }


}



