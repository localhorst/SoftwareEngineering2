﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary01
{
    public class Class1 :IDisposable
    {


        public void DoIt()
        {
            var foo = SingeltonPattern.GetInstance();
        }

        public int Add(int i1, int i2 = 10)
        {
            return i1 + i2;
        }

        public int Add(int i1)
        {
            return Add(i1, 10);
        }

        private Pen _pen;

        public void Dispose()
        {
            _pen.Dispose();
        }


        public Pen getPen()
        {

            _pen = new Pen(Color.Black, 2);

            return _pen;
        }
    }
}
