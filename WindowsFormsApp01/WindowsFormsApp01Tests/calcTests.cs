﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WindowsFormsApp01;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp01.Tests
{
    [TestClass()]
    public class calcTests
    {
        [TestMethod()]
        public void ADDTestPositive()
        {
            Assert.AreEqual(calc.ADD(1, 2), 3);
        }

        public void ADDTestNegative()
        {
            Assert.AreEqual(calc.ADD(-1, -2), -3);
        }
    }
}